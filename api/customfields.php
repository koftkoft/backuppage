<?php
//  DOC:  http://v2.wp-api.org/extending/modifying/

/**
 * Add the field "spaceship" to REST API responses for posts read and write
 */
add_action( 'rest_api_init', 'slug_register_fields' );
function slug_register_fields() {


	$fields = array( 
		"fb_link",
		"fb_source",
		"fb_id",
		"fb_type",
		"fb_created_time",
		"fb_picture",
		"fb_full_picture",
		"fb_object_id",
		"fb_shares",
		"fb_post_url"
	 );

	foreach( $fields as $item) {
		register_rest_field( 'post',
	        $item,
	        array(
	            'get_callback'    => 'slug_get_field',
	            'update_callback' => 'slug_update_field',
	            'schema'          => null,
	        )
   		);
	}

    
}
/**
 * Handler for getting custom field data.
 *
 * @since 0.1.0
 *
 * @param array $object The object from the response
 * @param string $field_name Name of field
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_field( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], $field_name, true );
}

/**
 * Handler for updating custom field data.
 *
 * @since 0.1.0
 *
 * @param mixed $value The value of the field
 * @param object $object The object from the response
 * @param string $field_name Name of field
 *
 * @return bool|int
 */
function slug_update_field( $value, $object, $field_name ) {
    if ( ! $value || ! is_string( $value ) ) {
        return;
    }

    return update_post_meta( $object->ID, $field_name, strip_tags( $value ) );

}