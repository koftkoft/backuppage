<?php
function theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );


    if ( is_page_template( 'page-getfb.php' ) ) {
    	wp_enqueue_script( 'ba-bbq-deparam', get_stylesheet_directory_uri().'/js/jquery.ba-bbq.js', array('jquery') );
		wp_enqueue_script( 'getfb', get_stylesheet_directory_uri().'/js/getfb.js', array('jquery','ba-bbq-deparam') );
		wp_enqueue_script( 'insertwp', get_stylesheet_directory_uri().'/js/insertwp.js', array('getfb') );

		wp_localize_script( 'insertwp', 'WP_API_Settings', array( 'root' => esc_url_raw( rest_url() ), 'nonce' => wp_create_nonce( 'wp_rest' ) ) );
		
	}

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

require 'api/customfields.php';


