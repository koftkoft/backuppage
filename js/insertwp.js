var tester,get_fb_and_inserts;

jQuery( document ).ready(function($){
	tester = function () {
		console.log("Tester");
	}


	get_fb_and_inserts = function () {
		//console.log( "Entering get_fb_posts_and_display", limit, total, params)

		limit = typeof limit !== 'undefined' ? limit : 50;
  		total = typeof total !== 'undefined' ? total : 999;
  		params = typeof params !== 'undefined' ? params : {  
				limit: limit,
				fields:"message,picture,full_picture,id,object_id,created_time,source,link,shares,type"
		    };

		postleft = total; //number of post left to fetch the data

		FB.api(
			'/Thaicalisthenics/posts', 
			'get', 
			params
			,
			function (response) {

		      if (response && !response.error) {
		       	//$('.entry-content').append( listed_response( response ) );
		       	listed_response( response, '.entry-content' )
		        //console.log( response );

		        
		        //check should I get next page?
		        total = total - limit;
		        if( total > 0 ) {//still there are somes post to do
		        	if( limit > total ) limit = total; // will not exceed the total
		        	//console.log( "NOW ", limit ,total)
		        	//get next page
			        nextPage = response.paging.next;
			        params = jQuery.deparam.querystring(nextPage);
			        params.limit = limit; // change limit according to param

			        get_fb_and_inserts( limit, total, params);


		        }

		      }

		    }			
		);	
		
	}

	function listed_response( res, add_to ) {
		alldata = "";
		

		$.each( res.data, function( index, obj ){
			//console.log( index);
			post_count++;
			str = "";

			str += '<div class="fb_listed" id="fb_'+obj.id + '" >';

			str += '<div class="fbheading"> <a target="_blank" href="' + obj.link + '" >' + post_count + ' -- ';
			str += obj.type + ' :: ' + obj.id + '</a></div>';
			str += '<div class="fbcreatedtime">' +  obj.created_time + '</div>';
			str += '<div class="fbimg" ><a target="_blank" href='+ obj.full_picture+'> <img src="' + obj.picture + '" >  </a></div>' 
			
			if ( obj.message ) {
				if( obj.message.substring ) {
					str += '<div class="fbcontent">' +   obj.message.substring(0,80) + '</div>';
				}
			}
			

			if( obj.source ){ 
				str += '<div class="fbsource"> <a target="_blank" href="' +  obj.source + '"> '+ obj.source  +'</div>';
			}

			str += "</div>"

			alldata += str;

			$( add_to ).append( str );
			add_to_wp( obj );

		} );//each
		
		return alldata;
	} //listed_response

	function add_to_wp( obj ) {
		
		post_format = ""; // change Facebook format to WP format
		switch (obj.type) {
			case 'photo':
				post_format = 'image'; break;
			case 'video':
				post_format = 'video'; break;
			default:
				post_format = 'standard';
		}

		
		var date = new Date( obj.created_time ); // Change Facebook time to ISO time
		

		title = date.toISOString();
		slug=date.toISOString();
		source = obj.source ? obj.source : "";

		if( obj.message ){
			title = obj.message.substring(0,80);
			slug = obj.message.substring(0,80);
		}
		

		the_data = {
		    	'date_gmt' : date.toISOString(),
		    	'title' : title,
		    	'content' : obj.message,
		    	'format' : post_format,
		    	'slug' : slug,
		    	'status' : 'publish',

		    	///FB Things
		    	///
		    	
		    	"fb_link" : obj.link,
				"fb_source" : source,
				"fb_id" : obj.id,
				"fb_type" : obj.type,
				"fb_created_time" : obj.created_time,
				"fb_picture" : obj.picture,
				"fb_full_picture" : obj.full_picture,
				"fb_object_id" : obj.object_id,
		        "fb_post_url" : "https://www.facebook.com/"+obj.id, // link to the post on FB
		        
		    }
		if( obj.shares ) the_data["fb_shares"] = obj.shares.count; // some posts might haven't been shared yet


		
		$.ajax( {
		    url: WP_API_Settings.root + 'wp/v2/posts',
		    method: 'POST',
		    beforeSend: function ( xhr ) {
		        xhr.setRequestHeader( 'X-WP-Nonce', WP_API_Settings.nonce );
		    },
		    data: the_data
		} ).done( function ( response ) {
			$( '#fb_'+obj.id).css( 'background-color', '#D7FFD7')
			console.log( "SUCCESS", response)
		    //console.log( response );
		} ).fail( function (jqXHR,textStatus,errorThrown  ) {
			$( '#fb_'+obj.id).css( 'background-color', '#red')
			console.log( "ERROR", the_data );
		}).always( function (response ) {

		});

	}//add_to_wp



});