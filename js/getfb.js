

jQuery( document ).ready(function($){
  $.ajaxSetup({ cache: true });
  $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
    FB.init({
      appId: '1034369396620536',
      version: 'v2.5', // or v2.0, v2.1, v2.2, v2.3
      status: true,
      xfbml: false
    });     

    //User  should log in with app's identity to before getting data
   	FB.getLoginStatus(function(response) {
	  if (response.status === 'connected') {
	    console.log('Logged in.');
	    //get_fb_posts_and_display();
	    init();
	  }
	  else {
	    FB.login(function(){
    		//get_fb_posts_and_display();
    		init();
   		 });
	  }
	});

  });

  function init() {

	  $("#get_fb_posts_and_display").attr("disabled", false);
	  $("#get_fb_and_insert").attr("disabled", false);
  }

  $("#get_fb_posts_and_display").attr("disabled", true);
  $("#get_fb_and_insert").attr("disabled", true);
  	

  $("#get_fb_posts_and_display").click( function(evt){
  	evt.preventDefault();
  	$(this).attr("disabled", true);
  	get_fb_posts_and_display();
  });

 	$("#get_fb_and_insert").click( function(evt){
 		evt.preventDefault();
  		$(this).attr("disabled", true);
 		get_fb_and_inserts();
 	} ) ;
	

  post_count = 0;

	function get_fb_posts_and_display(limit, total, params) {
	//console.log( "Entering get_fb_posts_and_display", limit, total, params)

		limit = typeof limit !== 'undefined' ? limit : 10;
  		total = typeof total !== 'undefined' ? total : 50;
  		params = typeof params !== 'undefined' ? params : {  
				limit: limit,
				fields:"message,picture,full_picture,id,object_id,created_time,source,link,shares,type"
		    };

		postleft = total;

		FB.api(
			'/Thaicalisthenics/posts', 
			'get', 
			params
			,
			function (response) {

		      if (response && !response.error) {
		       	$('.entry-content').append( extract_response( response ) );
		        //console.log( response );

		        
		        //check should I get next page?
		        total = total - limit;
		        if( total > 0 ) {//still there are somes post to do
		        	if( limit > total ) limit = total; // will not exceed the total
		        	//console.log( "NOW ", limit ,total)
		        	//get next page
			        nextPage = response.paging.next;
			        params = jQuery.deparam.querystring(nextPage);
			        params.limit = limit; // change limit according to param

			        get_fb_posts_and_display( limit, total, params);


		        }

		      }

		    }			
		);	
		
	}

	function extract_response( res ) {
		alldata = "";
		

		$.each( res.data, function( index, obj ){
			//console.log( index);
			post_count++;
			str = "";

			str += '<div class="fb_post" id="fb_'+obj.id + '" >';

			str += '<div class="fbheading"> <a target="_blank" href="' + obj.link + '" >' + post_count + ' -- ';
			str += obj.type + ' :: ' + obj.id + '</a></div>';
			str += '<div class="fbcreatedtime">' +  obj.created_time + '</div>';
			str += '<div class="fbimg" > <img src="' + obj.full_picture + '" ></div>' 
			str += '<div class="fbcontent">' +  obj.message + '</div>';

			if( obj.source ){ 
				str += '<div class="fbsource">' +  obj.source + '</div>';
			}

			str += "</div>"

			alldata += str;
		} );//each


		
		return alldata;
	}



});

